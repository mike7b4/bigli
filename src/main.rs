use std::env;
use std::usize;
fn main() {
    let args : Vec<String> = env::args().collect();
    for arg in &args[1..] {
        let u : usize;
        if arg.as_str().starts_with("0x") {
            u = usize::from_str_radix(&arg[2..], 16).expect("Expect hex without garbage characters!");
        }
        else {
            u = arg.parse().expect("Expect a decimal!");
        }
        println!("In: {:08X} ({}) swapped: {:08X} ({})", u, u, u.swap_bytes(), u.swap_bytes());
    }
}
